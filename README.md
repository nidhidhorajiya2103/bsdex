# BSDEX

List of Crypto Coins with last price, price movement, and sparkline chart with 24H, 7D, YTD period


## API Reference

#### Get items

```http
  https://www.bsdex.de/de/
```

| Parameter | Type     | Description                |
| :-------- | :------- | :------------------------- |
|none       | `none`   |     none |

#### Get chart data

```http
 https://widgets.bsdex.de/api/charts={base.id}
```

| Parameter | Type     | Description                       |
| :-------- | :------- | :-------------------------------- |
| `id`      | `string` | base.id of item to fetch chart data |



## Deployment

To deploy this project run

step:1

```bash
  npm install --legacy-peer-deps
```
step:2
```bash
  cd ios && pod install
```

To run this project
```bash
  npx react-native run-ios
```
## Features

- Live Crypto currency data 
- Live Crypto currency chart with price movement 
- Cross platform

